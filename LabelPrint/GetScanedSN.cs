﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace LabelPrint
{
    public partial class GetScanedSN : Form
    {
        public string ScannedSN = string.Empty;

        public GetScanedSN()
        {
            InitializeComponent();
        }

        private void bDone_Click(object sender, EventArgs e)
        {
            ScannedSN = tbScannedSN.Text;
            DialogResult = DialogResult.OK;
        }
    }
}
