﻿namespace LabelPrint
{
    partial class EditPFSConfig
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tbServer = new System.Windows.Forms.TextBox();
            this.tbWorkCenter = new System.Windows.Forms.TextBox();
            this.tbDatabase = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.bSave = new System.Windows.Forms.Button();
            this.bCancel = new System.Windows.Forms.Button();
            this.cbPFSEnabled = new System.Windows.Forms.CheckBox();
            this.SuspendLayout();
            // 
            // tbServer
            // 
            this.tbServer.Location = new System.Drawing.Point(44, 64);
            this.tbServer.Name = "tbServer";
            this.tbServer.Size = new System.Drawing.Size(279, 26);
            this.tbServer.TabIndex = 0;
            // 
            // tbWorkCenter
            // 
            this.tbWorkCenter.Location = new System.Drawing.Point(44, 182);
            this.tbWorkCenter.Name = "tbWorkCenter";
            this.tbWorkCenter.Size = new System.Drawing.Size(279, 26);
            this.tbWorkCenter.TabIndex = 2;
            // 
            // tbDatabase
            // 
            this.tbDatabase.Location = new System.Drawing.Point(44, 121);
            this.tbDatabase.Name = "tbDatabase";
            this.tbDatabase.Size = new System.Drawing.Size(279, 26);
            this.tbDatabase.TabIndex = 1;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(44, 41);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(55, 20);
            this.label1.TabIndex = 5;
            this.label1.Text = "Server";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(44, 98);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(79, 20);
            this.label2.TabIndex = 6;
            this.label2.Text = "Database";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(44, 159);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(98, 20);
            this.label3.TabIndex = 7;
            this.label3.Text = "Work Center";
            // 
            // bSave
            // 
            this.bSave.Location = new System.Drawing.Point(42, 308);
            this.bSave.Name = "bSave";
            this.bSave.Size = new System.Drawing.Size(75, 34);
            this.bSave.TabIndex = 3;
            this.bSave.Text = "Save";
            this.bSave.UseVisualStyleBackColor = true;
            this.bSave.Click += new System.EventHandler(this.bSave_Click);
            // 
            // bCancel
            // 
            this.bCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.bCancel.Location = new System.Drawing.Point(176, 308);
            this.bCancel.Name = "bCancel";
            this.bCancel.Size = new System.Drawing.Size(75, 34);
            this.bCancel.TabIndex = 4;
            this.bCancel.Text = "Cancel";
            this.bCancel.UseVisualStyleBackColor = true;
            // 
            // cbPFSEnabled
            // 
            this.cbPFSEnabled.AutoSize = true;
            this.cbPFSEnabled.Checked = true;
            this.cbPFSEnabled.CheckState = System.Windows.Forms.CheckState.Checked;
            this.cbPFSEnabled.Location = new System.Drawing.Point(44, 239);
            this.cbPFSEnabled.Name = "cbPFSEnabled";
            this.cbPFSEnabled.Size = new System.Drawing.Size(129, 24);
            this.cbPFSEnabled.TabIndex = 8;
            this.cbPFSEnabled.Text = "PFS Enabled";
            this.cbPFSEnabled.UseVisualStyleBackColor = true;
            // 
            // EditPFSConfig
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(520, 380);
            this.ControlBox = false;
            this.Controls.Add(this.cbPFSEnabled);
            this.Controls.Add(this.bCancel);
            this.Controls.Add(this.bSave);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.tbDatabase);
            this.Controls.Add(this.tbWorkCenter);
            this.Controls.Add(this.tbServer);
            this.Name = "EditPFSConfig";
            this.Text = "PFS System Setup";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox tbServer;
        private System.Windows.Forms.TextBox tbWorkCenter;
        private System.Windows.Forms.TextBox tbDatabase;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button bSave;
        private System.Windows.Forms.Button bCancel;
        private System.Windows.Forms.CheckBox cbPFSEnabled;
    }
}