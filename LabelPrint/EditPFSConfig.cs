﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace LabelPrint
{
    public partial class EditPFSConfig : Form
    {
        public string Server;
        public string DatabaseName;
        public string WorkCenter;
        public bool PFSEnabled;

        public EditPFSConfig(string server, string database, string workcenter, bool enabled)
        {
            InitializeComponent();
            Server = server;
            tbServer.Text = server;
            DatabaseName = database;
            tbDatabase.Text = database;
            WorkCenter = workcenter;
            tbWorkCenter.Text = workcenter;
            cbPFSEnabled.Checked = enabled;
            PFSEnabled = enabled;
        }

        private void bSave_Click(object sender, EventArgs e)
        {
            Server = tbServer.Text;
            DatabaseName = tbDatabase.Text;
            WorkCenter = tbWorkCenter.Text;
            PFSEnabled = cbPFSEnabled.Checked;
            DialogResult = DialogResult.OK;
        }
    }
}
