﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Management;
using System.IO.Ports;


namespace LabelPrint
{
    public partial class StationSetup : Form
    {
        public string SensorComPort = string.Empty;
        public string StationName = string.Empty;
        public bool ChangeoverEnabled = true;
        public string LabelDirectory = string.Empty;

        public StationSetup()
        {
            SensorComPort = Properties.Settings.Default.SensorCOMPort;
            StationName = Properties.Settings.Default.StationName;
            LabelDirectory = Properties.Settings.Default.LabelsDirectory;

            InitializeComponent();

            lbCurrentComPort.Text = Properties.Settings.Default.SensorCOMPort;
            tbStationName.Text = Properties.Settings.Default.StationName;
            tbLabelDirectory.Text = Properties.Settings.Default.LabelsDirectory;
        }


        // private utils
        private void DutComPort_DropDown(object sender, EventArgs e)
        {
            string[] ports = SerialPort.GetPortNames();         // get list of serial ports
            ComboBox mybutton = sender as ComboBox;

            mybutton.Items.Clear();

            try
            {
                ManagementObjectSearcher searcher =
                    new ManagementObjectSearcher("root\\CIMV2",
                    "SELECT * FROM Win32_PnPEntity");

                foreach (ManagementObject queryObj in searcher.Get())
                {
                    if (queryObj["Caption"] != null)
                    {
                        if (queryObj["Caption"].ToString().Contains("(COM"))
                        {
                            mybutton.Items.Add(queryObj["Caption"]);
                        }
                    }

                }
            }
            catch (ManagementException exx)
            {
                MessageBox.Show(exx.Message);
            }
        }

        private void bDone_Click(object sender, EventArgs e)
        {
            StationName = tbStationName.Text;
            Properties.Settings.Default.StationName = tbStationName.Text;
            Properties.Settings.Default.SensorCOMPort = SensorComPort;
            Properties.Settings.Default.Save();
            DialogResult = DialogResult.OK;
        }

        private void cbSensorComPort_SelectionChangeCommitted(object sender, EventArgs e)
        {
            int start;
            string temp;

            SensorComPort = cbSensorComPort.SelectedItem.ToString();            // get port desc from combobox
            start = SensorComPort.IndexOf("(COM") + 1;                          // strip off extra stuff
            temp = SensorComPort.Substring(start, SensorComPort.Length - start - 1);
            SensorComPort = temp;
            lbCurrentComPort.Text = temp;
        }

        private void bSelectLabelDirectory_Click(object sender, EventArgs e)
        {
            tbLabelDirectory.Text = Properties.Settings.Default.LabelsDirectory;
            FolderBrowserDialog folderDlg = new FolderBrowserDialog();
            folderDlg.ShowNewFolderButton = true;
            // Show the FolderBrowserDialog.  
            DialogResult result = folderDlg.ShowDialog();
            if (result == DialogResult.OK)
            {
                tbLabelDirectory.Text = folderDlg.SelectedPath;
                Properties.Settings.Default.LabelsDirectory = tbLabelDirectory.Text;
            }
        }
    }
}
