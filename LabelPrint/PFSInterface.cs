﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.InteropServices;
using System.Windows.Forms;

using Pfs4;

namespace LabelPrint
{

        public class PFSInterface
    {
        // pfs stuff
        private ClassLibrary Test_Link = null;


        // error codes
        const int Errors_BlankUserOrPassword = -1;          // User or Password was blank
        const int Errors_LoginFailed = -2;                  // login failed
        const int Errors_QueryFailed = -3;                  // query for sn failed
        const int Errors_PFSDLLFailed = -4;                 // DLL threw a exception
        const int Errors_MissingInfomation = -5;            // some of the required info is missing
        const int Errors_SendingResult = -6;                // error sending the result
        const int Errors_SN_Rejected = -7;                  // sn was rejected by query

        private string PFSaddress = string.Empty;
        private int bufSize = 1024;
        private string database = string.Empty;
        private string assemblyNum;
 //       private string serialNumber;
        private string workCenter;

        public string User = string.Empty;
        public string Password = string.Empty;
        public string OperationCode;


        public PFSInterface(string serverPFSaddress, string databaseName, string workcenter)
        {
            this.database = databaseName;
            this.PFSaddress = serverPFSaddress;
            this.workCenter = workcenter;
            Test_Link = new ClassLibrary();
        }


        public bool IsPFSInitialized { get { return !((PFSaddress == string.Empty)|(database == string.Empty)|(workCenter == string.Empty)); } }


        /// <summary>
        /// Log into PFS. Will return true if successful.
        /// </summary>
        /// <param name="database">database name to connect to</param>
        /// <param name="user">user for database</param>
        /// <param name="password">password for database</param>
        /// <param name="address">database IP address</param>
        /// <param name="pfs_message">Possible pfs_messages:
        ///                         OK[CR+LF{requested data}]
        ///                         {procedure name} Warning: {warning message}
        ///                         {procedure name} Failure: {failure message}
        ///                         {procedure name} Error: {error message}
        ///                         DLL Error: {error message}
        /// <returns>true = successful</returns>
        public void PSFLogin(out bool errorOccurred, out int errorCode, out String errorMsg)
        {
            string request = string.Empty;
            string pfs_message = string.Empty;
            StringBuilder rntStr = new StringBuilder(bufSize);

            if ((User == string.Empty) | (Password == string.Empty))
            {
                errorMsg = "User and Password can not be blank.";
                errorOccurred = true;
                errorCode = Errors_BlankUserOrPassword;
            }
            try
            {
                if (database.ToUpper() == "DUMMY")          // if doing fake login, not socket.dll
                {
                    var yn = MessageBox.Show(request, "login request for " + User, MessageBoxButtons.YesNo);
                    if (yn == DialogResult.Yes)
                    {
                        errorMsg  = string.Empty;
                        errorCode = 0;
                        errorOccurred = false;
                        return;
                    }
                    else
                    {
                        errorMsg = "Unable to log in";
                        errorCode = Errors_LoginFailed;
                        errorOccurred = true;
                        return;
                    }
                }
                else        // real login
                {
                    // construct the request
                    request = string.Format("REQUEST_TYPE=PfsVerifyUserInput\nDATABASE={0}\nUSER_ID={1}\nPASSWORD={2}\n\n", database, User, Password);
                    pfs_message = Test_Link.SendRequest(PFSaddress, request);
//                    lclPFSLogin(request, rntStr, bufSize, PFSaddress);
//                    pfs_message = rntStr.ToString();
                    if (pfs_message.Contains("OK"))
                    {
                        errorMsg = string.Empty;
                        errorCode = 0;
                        errorOccurred = false;
                        return;
                    }
                    else
                    {
                        errorMsg = pfs_message;
                        errorCode = Errors_LoginFailed;
                        errorOccurred = true;
                        return;
                    }
                }
            }
            catch (Exception ex)
            {
                errorMsg = "PSF Login: " + string.Format("DLL Error: {0}", ex.Message);
                errorCode = Errors_PFSDLLFailed;
                errorOccurred = true;
            }

            return;
        }

        /// <summary>
        /// Does a query to the PFS to see if this SN can be run.
        /// </summary>
        /// <param name="assemblyNum"></param>
        /// <param name="serialNumber"></param>
        /// <param name="operationCode"></param>
        /// <returns></returns>
        public bool PSFQuery(string assemblyNum, string serialNumber, out bool errorOccurred, out int errorCode, out String errorMsg)
        {
            string request = string.Empty;
            string pfs_message = string.Empty;
            StringBuilder rntStr = new StringBuilder(bufSize);
            bool passed = true;

            // Verify that it has user info
            if ((User == string.Empty) | (Password == string.Empty))
            {
                errorMsg = "PFSQuery: User or Password is blank.";
                errorCode = Errors_BlankUserOrPassword;
                errorOccurred = true;
                return true;            // test will fail because of above system error
            }

            this.assemblyNum = assemblyNum;
//            this.serialNumber = serialNumber;

            if (database.ToUpper() == "DUMMY")
            {
                var yn = MessageBox.Show(request + "\n" +serialNumber + "query request pass?", "Log in", MessageBoxButtons.YesNo);
                if (yn == DialogResult.No)
                {
                    errorMsg = "PFSQuery: failed for " + serialNumber;
                    errorCode = Errors_QueryFailed;
                    errorOccurred = false;
                    return false;            // test will fail because of above error
                }
                else
                {
                    errorMsg = "PFSQuery: passed for " + serialNumber;
                    errorCode = 0;
                    errorOccurred = false;
                    return true;
                }
            }
            else
            {
                // construct the request. Use the database info from when PSFLogin was called
                request = string.Format("REQUEST_TYPE=PfsQuery\nDATABASE={0}\nUSER_ID={1}\nPASSWORD={2}\nASSEMBLY_NUM={3}\nSERIAL_NUM={4}\nOPERATION_CODE={5}\n\n",
                    database, User, Password, assemblyNum, serialNumber, OperationCode);
                try
                {
                    pfs_message = Test_Link.SendRequest(PFSaddress, request);
                    errorMsg = "Request: " + request + "Result: " + pfs_message;            // use this string to return the pfs_message
                    errorCode = 0;
                    errorOccurred = false;
                   if (!pfs_message.Contains("OK"))
                    {
                        passed = false;
                        errorCode = Errors_SN_Rejected;
                        errorOccurred = false;              // set to false because not a system error
                    }
                }
                catch (Exception ex)
                {
                    errorMsg = "PFSQuery: failed. " + string.Format("DLL Error: {0}", ex.Message);
                    errorCode = Errors_PFSDLLFailed;
                    errorOccurred = true;
                }
            }


            return passed;
        }

        /// <summary>
        /// Will send the results to PFS. Assumes that PSFQuery was already run for the part. This will use
        /// data from PFSQuery and PSFLogin. It will only fail with system failures.
        /// </summary>
        /// <param name="workCenter"></param>
        /// <param name="passFail"></param>
        /// <param name="pfs_message"></param>
        /// <returns></returns>
        public void PSFSendResults(bool passed, string sn, out bool errorOccurred, out int errorCode, out String errorMsg)
        {
            string request = string.Empty;
            StringBuilder rntStr = new StringBuilder(bufSize);
            string passFailstr;
            string pfs_message;

            // Verify that the database stuff is set up
            if ((assemblyNum == string.Empty) | (sn == string.Empty) | (OperationCode == string.Empty))
            {
                errorCode = Errors_MissingInfomation;
                errorMsg  = "Missing information. Make sure PSFQuery has been run";
                errorOccurred = true;
                return;
            }

            passFailstr = passed ? "P" : "F";

            // construct the request. Use the database info from when PSFLogin and PSFQuery was called
            request = string.Format("REQUEST_TYPE=PfsSendResults\nDATABASE={0}\nUSER_ID={1}\nPASSWORD={2}\nASSEMBLY_NUM={3}\nSERIAL_NUM={4}\nOPERATION_CODE={5}\nWORK_CENTER={6}\nPASS_FAIL={7}\n\n",
                database, User, Password, assemblyNum, sn, OperationCode, workCenter, passFailstr);
            try
            {
                if (database.ToUpper() == "DUMMY")
                {
                    DialogResult yn = MessageBox.Show("Result for: " + assemblyNum + ":" + sn + ":" + OperationCode + ":" + workCenter + ":" + passFailstr,"Send Results",  MessageBoxButtons.YesNo);
                    if (yn == DialogResult.Yes)
                    {
                        errorCode = 0;
                        errorMsg = string.Empty;
                        errorOccurred = false;
                    }
                    else
                    {
                        errorCode = Errors_SendingResult;
                        errorMsg = "Error sending the result.";
                        errorOccurred = true;
                    }
                }
                else
                {
                    pfs_message = Test_Link.SendRequest(PFSaddress, request);
                    if (pfs_message.ToUpper().Contains("OK"))
                    {
                        errorCode = 0;
                        errorMsg = "Request: " + request + "Result: " + pfs_message;
                        errorOccurred = false;
                    }
                    else
                    {
                        errorCode = Errors_SendingResult;
                        errorMsg = "Error sending the result. Request: " + request + "Result: " + pfs_message;
                        errorOccurred = true;
                    }
                }
            }
            catch (Exception ex)
            {
                errorCode = Errors_SendingResult;
                errorMsg = "Error sending the result. " + ex.Message;
                errorOccurred = true;
            }
        }

        /// <summary>
        /// Will send the results to PFS. Assumes that PSFQuery was already run for the part. This will use
        /// data from PFSQuery and PSFLogin. It will only fail with system failures.
        /// </summary>
        /// <param name="workCenter"></param>
        /// <param name="passFail"></param>
        /// <param name="pfs_message"></param>
        /// <returns></returns>
        public void PSFSendResults2(bool passed, string sn, int failureCode, string failureMsg, out bool errorOccurred, out int errorCode, out String errorMsg)
        {
            string request = string.Empty;
            StringBuilder rntStr = new StringBuilder(bufSize);
            string passFailstr;
            string pfs_message;

            // Verify that the database stuff is set up
            if ((assemblyNum == string.Empty) | (sn == string.Empty) | (OperationCode == string.Empty))
            {
                errorCode = Errors_MissingInfomation;
                errorMsg = "Missing information. Make sure PSFQuery has been run";
                errorOccurred = true;
                return;
            }

            passFailstr = passed ? "P" : "F";

            // construct the request. Use the database info from when PSFLogin and PSFQuery was called
            request = string.Format("REQUEST_TYPE=PfsSendResults\nDATABASE={0}\nUSER_ID={1}\nPASSWORD={2}\nASSEMBLY_NUM={3}\nSERIAL_NUM={4}\nOPERATION_CODE={5}\nWORK_CENTER={6}\nPASS_FAIL={7}\n",
                                        database, User, Password, assemblyNum, sn, OperationCode, workCenter, passFailstr);
            if (passed)
                request += "\n";
            else
                request += "DEFECT_FIELDS=FAILURE_CODE;FAILURE_COMMENT\nDEFECTS=[" + failureCode + ";" + failureMsg + "]\n\n";
            try
            {
                if (database.ToUpper() == "DUMMY")
                {
                    DialogResult yn = MessageBox.Show(request, "Send Request", MessageBoxButtons.YesNo);
                    if (yn == DialogResult.Yes)
                    {
                        errorCode = 0;
                        errorMsg = string.Empty;
                        errorOccurred = false;
                    }
                    else
                    {
                        errorCode = Errors_SendingResult;
                        errorMsg = "Error sending the result." + request;
                        errorOccurred = true;
                    }
                }
                else
                {
                    pfs_message = Test_Link.SendRequest(PFSaddress, request);
                    if (pfs_message.ToUpper().Contains("OK"))
                    {
                        errorCode = 0;
                        errorMsg = "Request: " + request + "Result: " + pfs_message;
                        errorOccurred = false;
                    }
                    else
                    {
                        errorCode = Errors_SendingResult;
                        errorMsg = "Error sending the result. Request: " + request + "Result: " + pfs_message;
                        errorOccurred = true;
                    }
                }
            }
            catch (Exception ex)
            {
                errorCode = Errors_SendingResult;
                errorMsg = "Error sending the result. " + ex.Message;
                errorOccurred = true;
            }
        }

    }
}
