﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace LabelPrint
{
    public partial class ServerSetup : Form
    {
        public string Server;
        public string SNDatabaseName;
        public string ProductDatabaseName;
        public string User;
        public string Password;

        public ServerSetup()
        {
            InitializeComponent();
            tbServer.Text = Properties.Settings.Default.ENLServer;
            tbSNDBName.Text = Properties.Settings.Default.ENLSerialNumberDBName;
            tbProductDBName.Text = Properties.Settings.Default.ENLProductmapDBName;
            tbUser.Text = Properties.Settings.Default.ENLServerUser;
            tbPassword.Text = Properties.Settings.Default.ENLServerPassword;
        }

        private void bDone_Click(object sender, EventArgs e)
        {
            Server = tbServer.Text;
            SNDatabaseName = tbSNDBName.Text;
            ProductDatabaseName = tbProductDBName.Text;
            User = tbUser.Text;
            Password = tbPassword.Text;
            DialogResult = DialogResult.OK;
        }
    }
}
