﻿namespace LabelPrint
{
    partial class GetScanedSN
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tbScannedSN = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.bDone = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // tbScannedSN
            // 
            this.tbScannedSN.Location = new System.Drawing.Point(101, 126);
            this.tbScannedSN.Name = "tbScannedSN";
            this.tbScannedSN.Size = new System.Drawing.Size(410, 26);
            this.tbScannedSN.TabIndex = 0;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(92, 99);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(72, 20);
            this.label1.TabIndex = 1;
            this.label1.Text = "Scan SN";
            // 
            // bDone
            // 
            this.bDone.Location = new System.Drawing.Point(413, 252);
            this.bDone.Name = "bDone";
            this.bDone.Size = new System.Drawing.Size(98, 35);
            this.bDone.TabIndex = 2;
            this.bDone.Text = "Done";
            this.bDone.UseVisualStyleBackColor = true;
            this.bDone.Click += new System.EventHandler(this.bDone_Click);
            // 
            // GetScanedSN
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.bDone);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.tbScannedSN);
            this.Name = "GetScanedSN";
            this.Text = "GetScanedSN";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox tbScannedSN;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button bDone;
    }
}