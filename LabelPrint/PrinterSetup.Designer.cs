﻿namespace LabelPrint
{
    partial class PrinterSetup
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.cbPcbaPrinter = new System.Windows.Forms.ComboBox();
            this.cbHlaPrinter = new System.Windows.Forms.ComboBox();
            this.bDone = new System.Windows.Forms.Button();
            this.lbPcbaPrinter = new System.Windows.Forms.Label();
            this.lbHlaPrinter = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(46, 66);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(102, 20);
            this.label1.TabIndex = 0;
            this.label1.Text = "PCBA Printer";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(46, 135);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(91, 20);
            this.label2.TabIndex = 1;
            this.label2.Text = "HLA Printer";
            // 
            // cbPcbaPrinter
            // 
            this.cbPcbaPrinter.FormattingEnabled = true;
            this.cbPcbaPrinter.Location = new System.Drawing.Point(180, 63);
            this.cbPcbaPrinter.Name = "cbPcbaPrinter";
            this.cbPcbaPrinter.Size = new System.Drawing.Size(484, 28);
            this.cbPcbaPrinter.TabIndex = 2;
            this.cbPcbaPrinter.SelectedIndexChanged += new System.EventHandler(this.cbPcbaPrinter_SelectedIndexChanged);
            // 
            // cbHlaPrinter
            // 
            this.cbHlaPrinter.FormattingEnabled = true;
            this.cbHlaPrinter.Location = new System.Drawing.Point(180, 135);
            this.cbHlaPrinter.Name = "cbHlaPrinter";
            this.cbHlaPrinter.Size = new System.Drawing.Size(484, 28);
            this.cbHlaPrinter.TabIndex = 3;
            this.cbHlaPrinter.SelectedIndexChanged += new System.EventHandler(this.cbHlaPrinter_SelectedIndexChanged);
            // 
            // bDone
            // 
            this.bDone.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.bDone.Location = new System.Drawing.Point(180, 216);
            this.bDone.Name = "bDone";
            this.bDone.Size = new System.Drawing.Size(75, 38);
            this.bDone.TabIndex = 4;
            this.bDone.Text = "Done";
            this.bDone.UseVisualStyleBackColor = true;
            // 
            // lbPcbaPrinter
            // 
            this.lbPcbaPrinter.AutoSize = true;
            this.lbPcbaPrinter.Location = new System.Drawing.Point(180, 37);
            this.lbPcbaPrinter.Name = "lbPcbaPrinter";
            this.lbPcbaPrinter.Size = new System.Drawing.Size(109, 20);
            this.lbPcbaPrinter.TabIndex = 5;
            this.lbPcbaPrinter.Text = "none selected";
            // 
            // lbHlaPrinter
            // 
            this.lbHlaPrinter.AutoSize = true;
            this.lbHlaPrinter.Location = new System.Drawing.Point(180, 109);
            this.lbHlaPrinter.Name = "lbHlaPrinter";
            this.lbHlaPrinter.Size = new System.Drawing.Size(109, 20);
            this.lbHlaPrinter.TabIndex = 6;
            this.lbHlaPrinter.Text = "none selected";
            // 
            // PrinterSetup
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 291);
            this.Controls.Add(this.lbHlaPrinter);
            this.Controls.Add(this.lbPcbaPrinter);
            this.Controls.Add(this.bDone);
            this.Controls.Add(this.cbHlaPrinter);
            this.Controls.Add(this.cbPcbaPrinter);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Name = "PrinterSetup";
            this.Text = "Printer Setup";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ComboBox cbPcbaPrinter;
        private System.Windows.Forms.ComboBox cbHlaPrinter;
        private System.Windows.Forms.Button bDone;
        private System.Windows.Forms.Label lbPcbaPrinter;
        private System.Windows.Forms.Label lbHlaPrinter;
    }
}