﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace LabelPrint
{
    public partial class GetSigninInfo : Form
    {
        public string User;
        public string Password;
        public string OperationCode;

        public GetSigninInfo(string user, string password, string operationcode)
        {
            InitializeComponent();
            User = user;
            tbUser.Text = user;
            Password = password;
            OperationCode = operationcode;
            tbOperationCode.Text = operationcode;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            User = tbUser.Text;
            Password = tbPassword.Text;
            OperationCode = tbOperationCode.Text;
            DialogResult = DialogResult.OK;
        }
    }
}
