﻿namespace LabelPrint
{
    partial class StationSetup
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.cbSensorComPort = new System.Windows.Forms.ComboBox();
            this.bDone = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.tbStationName = new System.Windows.Forms.TextBox();
            this.folderBrowserDialog1 = new System.Windows.Forms.FolderBrowserDialog();
            this.tbLabelDirectory = new System.Windows.Forms.TextBox();
            this.bSelectLabelDirectory = new System.Windows.Forms.Button();
            this.lbCurrentComPort = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(64, 53);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(133, 20);
            this.label1.TabIndex = 0;
            this.label1.Text = "Sensor COM Port";
            // 
            // cbSensorComPort
            // 
            this.cbSensorComPort.FormattingEnabled = true;
            this.cbSensorComPort.Location = new System.Drawing.Point(202, 76);
            this.cbSensorComPort.Name = "cbSensorComPort";
            this.cbSensorComPort.Size = new System.Drawing.Size(436, 28);
            this.cbSensorComPort.TabIndex = 1;
            this.cbSensorComPort.SelectionChangeCommitted += new System.EventHandler(this.cbSensorComPort_SelectionChangeCommitted);
            this.cbSensorComPort.Click += new System.EventHandler(this.DutComPort_DropDown);
            // 
            // bDone
            // 
            this.bDone.Location = new System.Drawing.Point(473, 353);
            this.bDone.Name = "bDone";
            this.bDone.Size = new System.Drawing.Size(75, 37);
            this.bDone.TabIndex = 2;
            this.bDone.Text = "Done";
            this.bDone.UseVisualStyleBackColor = true;
            this.bDone.Click += new System.EventHandler(this.bDone_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(52, 129);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(104, 20);
            this.label2.TabIndex = 3;
            this.label2.Text = "Station name";
            // 
            // tbStationName
            // 
            this.tbStationName.Location = new System.Drawing.Point(204, 122);
            this.tbStationName.Name = "tbStationName";
            this.tbStationName.Size = new System.Drawing.Size(220, 26);
            this.tbStationName.TabIndex = 4;
            this.tbStationName.Text = "FCT2";
            // 
            // tbLabelDirectory
            // 
            this.tbLabelDirectory.Location = new System.Drawing.Point(244, 172);
            this.tbLabelDirectory.Name = "tbLabelDirectory";
            this.tbLabelDirectory.Size = new System.Drawing.Size(537, 26);
            this.tbLabelDirectory.TabIndex = 11;
            // 
            // bSelectLabelDirectory
            // 
            this.bSelectLabelDirectory.Location = new System.Drawing.Point(56, 160);
            this.bSelectLabelDirectory.Name = "bSelectLabelDirectory";
            this.bSelectLabelDirectory.Size = new System.Drawing.Size(182, 50);
            this.bSelectLabelDirectory.TabIndex = 12;
            this.bSelectLabelDirectory.Text = "Select label directory";
            this.bSelectLabelDirectory.UseVisualStyleBackColor = true;
            this.bSelectLabelDirectory.Click += new System.EventHandler(this.bSelectLabelDirectory_Click);
            // 
            // lbCurrentComPort
            // 
            this.lbCurrentComPort.AutoSize = true;
            this.lbCurrentComPort.Location = new System.Drawing.Point(204, 52);
            this.lbCurrentComPort.Name = "lbCurrentComPort";
            this.lbCurrentComPort.Size = new System.Drawing.Size(19, 20);
            this.lbCurrentComPort.TabIndex = 13;
            this.lbCurrentComPort.Text = "--";
            // 
            // StationSetup
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.lbCurrentComPort);
            this.Controls.Add(this.bSelectLabelDirectory);
            this.Controls.Add(this.tbLabelDirectory);
            this.Controls.Add(this.tbStationName);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.bDone);
            this.Controls.Add(this.cbSensorComPort);
            this.Controls.Add(this.label1);
            this.Name = "StationSetup";
            this.Text = "Station Setup";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox cbSensorComPort;
        private System.Windows.Forms.Button bDone;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox tbStationName;
        private System.Windows.Forms.FolderBrowserDialog folderBrowserDialog1;
        private System.Windows.Forms.TextBox tbLabelDirectory;
        private System.Windows.Forms.Button bSelectLabelDirectory;
        private System.Windows.Forms.Label lbCurrentComPort;
    }
}