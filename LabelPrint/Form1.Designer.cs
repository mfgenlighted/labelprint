﻿namespace LabelPrint
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.button1 = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.lbMac = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.lbHLASN = new System.Windows.Forms.Label();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.label1 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.lbPCBASN = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.configureToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.serversToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.pFSToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.printersToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.stationToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.productSetupToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.tscbProducts = new System.Windows.Forms.ToolStripComboBox();
            this.aboutToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.label2 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.lbHLAPrinterLabelFile = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.lbPCBAPrinterFile = new System.Windows.Forms.Label();
            this.lbProduct = new System.Windows.Forms.Label();
            this.rtbStatus = new System.Windows.Forms.RichTextBox();
            this.lbPFSStatus = new System.Windows.Forms.Label();
            this.lbPFSAssembly = new System.Windows.Forms.Label();
            this.lbChangeoverOption = new System.Windows.Forms.Label();
            this.bReprint = new System.Windows.Forms.Button();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(70, 66);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(158, 56);
            this.button1.TabIndex = 4;
            this.button1.Text = "Read sensor data";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(290, 102);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(44, 20);
            this.label3.TabIndex = 10;
            this.label3.Text = "MAC";
            // 
            // lbMac
            // 
            this.lbMac.AutoSize = true;
            this.lbMac.Location = new System.Drawing.Point(347, 102);
            this.lbMac.Name = "lbMac";
            this.lbMac.Size = new System.Drawing.Size(19, 20);
            this.lbMac.TabIndex = 11;
            this.lbMac.Text = "--";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(290, 130);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(41, 20);
            this.label5.TabIndex = 12;
            this.label5.Text = "HLA";
            // 
            // lbHLASN
            // 
            this.lbHLASN.AutoSize = true;
            this.lbHLASN.Location = new System.Drawing.Point(347, 130);
            this.lbHLASN.Name = "lbHLASN";
            this.lbHLASN.Size = new System.Drawing.Size(19, 20);
            this.lbHLASN.TabIndex = 13;
            this.lbHLASN.Text = "--";
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.FileName = "openFileDialog1";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(294, 57);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(91, 20);
            this.label1.TabIndex = 22;
            this.label1.Text = "Found data";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(294, 162);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(52, 20);
            this.label9.TabIndex = 23;
            this.label9.Text = "PCBA";
            // 
            // lbPCBASN
            // 
            this.lbPCBASN.AutoSize = true;
            this.lbPCBASN.Location = new System.Drawing.Point(351, 162);
            this.lbPCBASN.Name = "lbPCBASN";
            this.lbPCBASN.Size = new System.Drawing.Size(19, 20);
            this.lbPCBASN.TabIndex = 24;
            this.lbPCBASN.Text = "--";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(66, 184);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(56, 20);
            this.label10.TabIndex = 25;
            this.label10.Text = "Status";
            // 
            // menuStrip1
            // 
            this.menuStrip1.ImageScalingSize = new System.Drawing.Size(24, 24);
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.configureToolStripMenuItem,
            this.productSetupToolStripMenuItem,
            this.aboutToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(769, 33);
            this.menuStrip1.TabIndex = 26;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // configureToolStripMenuItem
            // 
            this.configureToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.serversToolStripMenuItem,
            this.pFSToolStripMenuItem,
            this.printersToolStripMenuItem,
            this.stationToolStripMenuItem});
            this.configureToolStripMenuItem.Name = "configureToolStripMenuItem";
            this.configureToolStripMenuItem.Size = new System.Drawing.Size(102, 29);
            this.configureToolStripMenuItem.Text = "Configure";
            // 
            // serversToolStripMenuItem
            // 
            this.serversToolStripMenuItem.Name = "serversToolStripMenuItem";
            this.serversToolStripMenuItem.Size = new System.Drawing.Size(155, 30);
            this.serversToolStripMenuItem.Text = "Servers";
            this.serversToolStripMenuItem.Click += new System.EventHandler(this.serversToolStripMenuItem_Click);
            // 
            // pFSToolStripMenuItem
            // 
            this.pFSToolStripMenuItem.Name = "pFSToolStripMenuItem";
            this.pFSToolStripMenuItem.Size = new System.Drawing.Size(155, 30);
            this.pFSToolStripMenuItem.Text = "PFS";
            this.pFSToolStripMenuItem.Click += new System.EventHandler(this.pFSToolStripMenuItem_Click);
            // 
            // printersToolStripMenuItem
            // 
            this.printersToolStripMenuItem.Name = "printersToolStripMenuItem";
            this.printersToolStripMenuItem.Size = new System.Drawing.Size(155, 30);
            this.printersToolStripMenuItem.Text = "Printers";
            this.printersToolStripMenuItem.Click += new System.EventHandler(this.printersToolStripMenuItem_Click);
            // 
            // stationToolStripMenuItem
            // 
            this.stationToolStripMenuItem.Name = "stationToolStripMenuItem";
            this.stationToolStripMenuItem.Size = new System.Drawing.Size(155, 30);
            this.stationToolStripMenuItem.Text = "Station";
            this.stationToolStripMenuItem.Click += new System.EventHandler(this.stationToolStripMenuItem_Click);
            // 
            // productSetupToolStripMenuItem
            // 
            this.productSetupToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tscbProducts});
            this.productSetupToolStripMenuItem.Name = "productSetupToolStripMenuItem";
            this.productSetupToolStripMenuItem.Size = new System.Drawing.Size(137, 29);
            this.productSetupToolStripMenuItem.Text = "Product Setup";
            this.productSetupToolStripMenuItem.Click += new System.EventHandler(this.productSetupToolStripMenuItem_Click);
            // 
            // tscbProducts
            // 
            this.tscbProducts.DropDownWidth = 300;
            this.tscbProducts.Name = "tscbProducts";
            this.tscbProducts.Size = new System.Drawing.Size(300, 33);
            this.tscbProducts.Sorted = true;
            this.tscbProducts.SelectedIndexChanged += new System.EventHandler(this.tscbProducts_SelectedIndexChanged);
            // 
            // aboutToolStripMenuItem
            // 
            this.aboutToolStripMenuItem.Name = "aboutToolStripMenuItem";
            this.aboutToolStripMenuItem.Size = new System.Drawing.Size(74, 29);
            this.aboutToolStripMenuItem.Text = "About";
            this.aboutToolStripMenuItem.Click += new System.EventHandler(this.aboutToolStripMenuItem_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(52, 453);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(136, 20);
            this.label2.TabIndex = 27;
            this.label2.Text = "Use Label files for";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(110, 526);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(91, 20);
            this.label4.TabIndex = 28;
            this.label4.Text = "HLA Printer";
            // 
            // lbHLAPrinterLabelFile
            // 
            this.lbHLAPrinterLabelFile.AutoSize = true;
            this.lbHLAPrinterLabelFile.Location = new System.Drawing.Point(232, 525);
            this.lbHLAPrinterLabelFile.Name = "lbHLAPrinterLabelFile";
            this.lbHLAPrinterLabelFile.Size = new System.Drawing.Size(24, 20);
            this.lbHLAPrinterLabelFile.TabIndex = 29;
            this.lbHLAPrinterLabelFile.Text = "---";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(403, 525);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(102, 20);
            this.label6.TabIndex = 30;
            this.label6.Text = "PCBA Printer";
            // 
            // lbPCBAPrinterFile
            // 
            this.lbPCBAPrinterFile.AutoSize = true;
            this.lbPCBAPrinterFile.Location = new System.Drawing.Point(533, 524);
            this.lbPCBAPrinterFile.Name = "lbPCBAPrinterFile";
            this.lbPCBAPrinterFile.Size = new System.Drawing.Size(24, 20);
            this.lbPCBAPrinterFile.TabIndex = 31;
            this.lbPCBAPrinterFile.Text = "---";
            // 
            // lbProduct
            // 
            this.lbProduct.AutoSize = true;
            this.lbProduct.Location = new System.Drawing.Point(192, 452);
            this.lbProduct.Name = "lbProduct";
            this.lbProduct.Size = new System.Drawing.Size(151, 20);
            this.lbProduct.TabIndex = 32;
            this.lbProduct.Text = "No product selected";
            // 
            // rtbStatus
            // 
            this.rtbStatus.Location = new System.Drawing.Point(47, 224);
            this.rtbStatus.Name = "rtbStatus";
            this.rtbStatus.ReadOnly = true;
            this.rtbStatus.ScrollBars = System.Windows.Forms.RichTextBoxScrollBars.Vertical;
            this.rtbStatus.Size = new System.Drawing.Size(640, 225);
            this.rtbStatus.TabIndex = 33;
            this.rtbStatus.Text = "";
            // 
            // lbPFSStatus
            // 
            this.lbPFSStatus.AutoSize = true;
            this.lbPFSStatus.Location = new System.Drawing.Point(490, 453);
            this.lbPFSStatus.Name = "lbPFSStatus";
            this.lbPFSStatus.Size = new System.Drawing.Size(197, 20);
            this.lbPFSStatus.TabIndex = 34;
            this.lbPFSStatus.Text = "PFS Status - Not logged in";
            // 
            // lbPFSAssembly
            // 
            this.lbPFSAssembly.AutoSize = true;
            this.lbPFSAssembly.Location = new System.Drawing.Point(490, 482);
            this.lbPFSAssembly.Name = "lbPFSAssembly";
            this.lbPFSAssembly.Size = new System.Drawing.Size(163, 20);
            this.lbPFSAssembly.TabIndex = 35;
            this.lbPFSAssembly.Text = "PFS Assembly - None";
            // 
            // lbChangeoverOption
            // 
            this.lbChangeoverOption.AutoSize = true;
            this.lbChangeoverOption.Location = new System.Drawing.Point(56, 482);
            this.lbChangeoverOption.Name = "lbChangeoverOption";
            this.lbChangeoverOption.Size = new System.Drawing.Size(19, 20);
            this.lbChangeoverOption.TabIndex = 36;
            this.lbChangeoverOption.Text = "--";
            // 
            // bReprint
            // 
            this.bReprint.Location = new System.Drawing.Point(70, 142);
            this.bReprint.Name = "bReprint";
            this.bReprint.Size = new System.Drawing.Size(158, 39);
            this.bReprint.TabIndex = 37;
            this.bReprint.Text = "Reprint Last Label";
            this.bReprint.UseVisualStyleBackColor = true;
            this.bReprint.Click += new System.EventHandler(this.bReprint_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(769, 596);
            this.Controls.Add(this.bReprint);
            this.Controls.Add(this.lbChangeoverOption);
            this.Controls.Add(this.lbPFSAssembly);
            this.Controls.Add(this.lbPFSStatus);
            this.Controls.Add(this.rtbStatus);
            this.Controls.Add(this.lbProduct);
            this.Controls.Add(this.lbPCBAPrinterFile);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.lbHLAPrinterLabelFile);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.lbPCBASN);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.lbHLASN);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.lbMac);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "Form1";
            this.Text = "Enlighted Label Print";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label lbMac;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label lbHLASN;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label lbPCBASN;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem configureToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem serversToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem pFSToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem printersToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem stationToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem productSetupToolStripMenuItem;
        private System.Windows.Forms.ToolStripComboBox tscbProducts;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label lbHLAPrinterLabelFile;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label lbPCBAPrinterFile;
        private System.Windows.Forms.Label lbProduct;
        private System.Windows.Forms.RichTextBox rtbStatus;
        private System.Windows.Forms.Label lbPFSStatus;
        private System.Windows.Forms.ToolStripMenuItem aboutToolStripMenuItem;
        private System.Windows.Forms.Label lbPFSAssembly;
        private System.Windows.Forms.Label lbChangeoverOption;
        private System.Windows.Forms.Button bReprint;
    }
}

