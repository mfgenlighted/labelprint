﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Drawing.Printing;

namespace LabelPrint
{
    public partial class PrinterSetup : Form
    {
        public string HlaPrinter = string.Empty;
        public string PcbaPrinter = string.Empty;

        public PrinterSetup(string pcbaPrinter, string hlaPrinter)
        {
            InitializeComponent();
            PrintDocument prtdoc = new PrintDocument();
            string defaultPrinter = prtdoc.PrinterSettings.PrinterName;     // get default printer name

            // if there are no printers specficied, set to the default printer
            if (hlaPrinter == string.Empty)
                HlaPrinter = defaultPrinter;
            else
                HlaPrinter = hlaPrinter;
            if (pcbaPrinter == string.Empty)
                PcbaPrinter = defaultPrinter;
            else
                PcbaPrinter = pcbaPrinter;

            lbHlaPrinter.Text = HlaPrinter;
            lbPcbaPrinter.Text = PcbaPrinter;

            // setup the combo boxes with the printer names
            foreach (string printer in PrinterSettings.InstalledPrinters)
            {
                cbHlaPrinter.Items.Add(printer);
                cbPcbaPrinter.Items.Add(printer);
                if (printer == HlaPrinter)      // set to selected or default printer
                    cbHlaPrinter.SelectedIndex = cbHlaPrinter.Items.IndexOf(HlaPrinter);
                if (printer == PcbaPrinter)
                    cbPcbaPrinter.SelectedIndex = cbPcbaPrinter.Items.IndexOf(PcbaPrinter);
            }
        }

        private void cbPcbaPrinter_SelectedIndexChanged(object sender, EventArgs e)
        {
            PcbaPrinter = cbPcbaPrinter.Items[cbPcbaPrinter.SelectedIndex].ToString();
        }

        private void cbHlaPrinter_SelectedIndexChanged(object sender, EventArgs e)
        {
            HlaPrinter = cbPcbaPrinter.Items[cbHlaPrinter.SelectedIndex].ToString();
        }
    }
}
