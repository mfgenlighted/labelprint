﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
using System.Windows.Forms;
using System.Drawing.Printing;
using System.Resources;
using System.IO;
using System.Text.RegularExpressions;

using SerialNumberFunctionsDLL;
using Seagull.BarTender.Print;
using PacketCommications;
using ProductDatabaseDLL;

namespace LabelPrint
{
    public partial class Form1 : Form
    {
        enum GetDataType { Read, Scan};     // where to get data from. Read=get from sensor, Scan=scan barcode

        string InputedSN = string.Empty;

        List<ProductDatabase.Product_data> productData = new List<ProductDatabase.Product_data>();
        ProductDatabase.Product_data selectedProducData = new ProductDatabase.Product_data();

        PFSInterface PFS = null;
        string PfsAssemblyNumber = string.Empty;

        public Form1()
        {
            InitializeComponent();
        }


 

        private void button1_Click(object sender, EventArgs e)
        {
            if (PfsAssemblyNumber == string.Empty)
            {
                MessageBox.Show("Product needs to be configured.");
                return;
            }
            if (selectedProducData.hw_code == (int)GetDataType.Scan)    // if configured to scan the serial number
            {
                string scannedSN = string.Empty;
                GetScanedSN iform = new GetScanedSN();
                iform.ShowDialog();
                scannedSN = iform.ScannedSN;
                iform.Dispose();
                if (scannedSN.Substring(1, 5).ToUpper() != selectedProducData.pcba_product_code)    // make sure correct product
                {
                    if (scannedSN.Substring(1, 5).ToUpper() != selectedProducData.product_code)     // maybe hla was scaned
                    {
                        MessageBox.Show("The product scanned does not match the product configured.");
                        return;
                    }
                }
                scanversion(scannedSN);
                return;
            }

            // falls to here if not configured to scan the serial number
            bool errorOccured = false;
            string errorMsg = string.Empty;
            string editedErrorMsg = string.Empty;
            int errorCode = 0;

            string fullLabelName = string.Empty;
            string pcbaSn = string.Empty;
            string pcbaPn = string.Empty;
            string hlaSn = string.Empty;
            string hlaPn = string.Empty;
            string mac1 = string.Empty;
            string mac2 = string.Empty;
            string model = string.Empty;

            rtbStatus.Clear();
            lbMac.Text = "";
            lbHLASN.Text = "";


            // open port
            PacketCommications.DVTMANCommandsHandler console = new DVTMANCommandsHandler();
            console.Open(Properties.Settings.Default.SensorCOMPort, out errorOccured, out errorCode, out errorMsg);
            if (errorOccured)
            {
                MessageBox.Show("Error opening port " + Properties.Settings.Default.SensorCOMPort);
                return;
            }

            // issue command to get pcba sn, hla sn, macs
            rtbStatus.AppendText("Reading sensor data\n");
            console.GetHLAData(out hlaPn, out hlaSn, out model, out errorOccured, out errorCode, out errorMsg);
            if (errorOccured)
            {
                MessageBox.Show("Error getting HLA data. " + errorMsg);
                console.Close();
                return;
            }
            if ((hlaPn.Length != 11) | (hlaSn.Length != 15))
            {
                MessageBox.Show("Error getting HLA data. Part number or serial number not the right lenght.");
                console.Close();
                return;
            }
            console.GetPCBAData(out pcbaPn, out pcbaSn, out errorOccured, out errorCode, out errorMsg);
            if (errorOccured)
            {
                MessageBox.Show("Error getting PCBA data. " + errorMsg);
                console.Close();
                return;
            }
            if ((pcbaPn.Length != 11) | (pcbaSn.Length != 15))
            {
                MessageBox.Show("Error getting PCBA data. Part number or serial number not the right lenght.");
                console.Close();
                return;
            }
            console.GetMACData(out mac1, out mac2, out errorOccured, out errorCode, out errorMsg);
            if (errorOccured)
            {
                MessageBox.Show("Error getting PCBA data. " + errorMsg);
                console.Close();
                return;
            }
            if ((mac1.Length != 12) | (mac2.Length != 12) | (mac1 == "FFFFFFFFFFFF") | (mac2 == "FFFFFFFFFFFF"))
            {
                MessageBox.Show("Error getting MAC data. Not the right lenght or FFFFFFFFFFFF.");
                console.Close();
                return;
            }

            lbHLASN.Text = hlaSn;
            lbPCBASN.Text = pcbaSn;
            lbMac.Text = mac1;

            // Check if OK to run
            if (Properties.Settings.Default.PFSEnabled)
            {
                if (!PFS.PSFQuery(lbPFSAssembly.Text, pcbaSn, out errorOccured, out errorCode, out errorMsg))
                {
                    editedErrorMsg = errorMsg.Substring(errorMsg.IndexOf("Result"));
                    MessageBox.Show("PFS did not return OK.\nChangeover will not be done.\n" + editedErrorMsg);
                    return;
                }
            }

            // do changeover
            if (selectedProducData.hw_code == (int)GetDataType.Read)        // if configured to do Read from sensor, then do changeover
            {
                rtbStatus.AppendText("Starting change over\n");
                if (!console.PreformChangeOver(3000, out errorCode, out errorOccured, out errorCode, out errorMsg))
                {
                    MessageBox.Show("Error doing the changeover command. " + errorMsg);
                    console.Close();
                    if (Properties.Settings.Default.PFSEnabled)
                    {
                        PFS.PSFSendResults(false, pcbaSn, out errorOccured, out errorCode, out errorMsg);
                        if (errorOccured)
                        {
                            MessageBox.Show("There was a error sending the PFS result.");
                            return;
                        }
                    }
                    return;
                }
                Thread.Sleep(3000);
                rtbStatus.AppendText("Finish change over. " + errorMsg + "\n");
            }
            else
                rtbStatus.AppendText("Changeover configured to skip.\n");
            console.Close();

            if (Properties.Settings.Default.PFSEnabled)
            {
                PFS.PSFSendResults(true, pcbaSn, out errorOccured, out errorCode, out errorMsg);
                if (errorOccured)
                {
                    MessageBox.Show("There was a error sending the PFS result.");
                    return;
                }
            }


            rtbStatus.AppendText("Label print started.\n");

            // print labels
            if (lbHLAPrinterLabelFile.Text != "")
            {
                fullLabelName = Properties.Settings.Default.LabelsDirectory + "\\" + lbHLAPrinterLabelFile.Text;
                PrintALabel(fullLabelName, Properties.Settings.Default.HlaPrinterName, pcbaSn, hlaSn, mac1, ref errorMsg);
                if (errorMsg != string.Empty)
                    MessageBox.Show("Error printing " + lbHLAPrinterLabelFile + " on printer " + Properties.Settings.Default.HlaPrinterName + "\n" + errorMsg);
            }
            if (lbPCBAPrinterFile.Text != "")
            {
                fullLabelName = Properties.Settings.Default.LabelsDirectory + "\\" + lbPCBAPrinterFile.Text;
                PrintALabel(fullLabelName, Properties.Settings.Default.PcbaPrinterName, pcbaSn, hlaSn, mac1, ref errorMsg);
                if (errorMsg != string.Empty)
                    MessageBox.Show("Error printing " + lbHLAPrinterLabelFile + " on printer " + Properties.Settings.Default.HlaPrinterName + "\n" + errorMsg);
            }
            rtbStatus.AppendText("Label print finished.\n");

        }

        void scanversion(string scannedSN)
        {
            bool errorOccured = false;
            int errorCode = 0;
            string pcbaSn = string.Empty;
            string hlaSn = string.Empty;
            string mac1 = string.Empty;
            string mac2 = string.Empty;
            string msg = string.Empty;
            string errorMsg = string.Empty;
            string editedErrorMsg = string.Empty;
            string fullLabelName = string.Empty;

            using (SerialNumberFunctions sndll = new SerialNumberFunctions(Properties.Settings.Default.ENLServer, Properties.Settings.Default.ENLSerialNumberDBName,
                    Properties.Settings.Default.ENLServerUser, Properties.Settings.Default.ENLServerPassword, "REPRT"))
            {
                if (!sndll.LookForPCBASN(scannedSN, ref hlaSn, ref mac1, ref mac2))
                {
                    // if pcba not found, maybe hla sn was scaned
                    if (!sndll.LookForHLASN(scannedSN, ref pcbaSn, ref mac1, ref mac2))
                    {
                        rtbStatus.AppendText("Error looking for serial number. " + sndll.LastErrorMessage + "\n");
                        return;
                    }
                    else
                    {
                        lbHLASN.Text = scannedSN;
                        hlaSn = scannedSN;
                        rtbStatus.AppendText("Found HLA serial number\n");
                        lbMac.Text = mac1;
                        lbPCBASN.Text = pcbaSn;
                    }
                }
                else
                {
                    rtbStatus.AppendText("Found PCBA serial number\n");
                    lbMac.Text = mac1;
                    lbHLASN.Text = hlaSn;
                    lbPCBASN.Text = scannedSN;
                    pcbaSn = scannedSN;
                }


                if (Properties.Settings.Default.PFSEnabled)
                {
                    if (!PFS.PSFQuery(lbPFSAssembly.Text, lbPCBASN.Text, out errorOccured, out errorCode, out errorMsg))
                    {
                        editedErrorMsg = errorMsg.Substring(errorMsg.IndexOf("Result"));
                        MessageBox.Show("PFS did not return OK.\nChangeover will not be done.\n" + editedErrorMsg);
                        return;
                    }
                }

                if (lbHLAPrinterLabelFile.Text != "")
                {
                    rtbStatus.AppendText("HLA print started.\n");
                    fullLabelName = Properties.Settings.Default.LabelsDirectory + "\\" + lbHLAPrinterLabelFile.Text;
                    PrintALabel(fullLabelName, Properties.Settings.Default.HlaPrinterName, pcbaSn, hlaSn, mac1, ref errorMsg);
                    rtbStatus.AppendText("Print done.\n");
                    if (errorMsg != string.Empty)
                        MessageBox.Show("Error printing " + lbHLAPrinterLabelFile.Text + " on printer " + Properties.Settings.Default.HlaPrinterName + "\n" + errorMsg);
                }
                if (lbPCBAPrinterFile.Text != "")
                {
                    rtbStatus.AppendText("PCBA print started.\n");
                    fullLabelName = Properties.Settings.Default.LabelsDirectory + "\\" + lbPCBAPrinterFile.Text;
                    PrintALabel(fullLabelName, Properties.Settings.Default.PcbaPrinterName, pcbaSn, hlaSn, mac1, ref errorMsg);
                    rtbStatus.AppendText("Print done.\n");
                    if (errorMsg != string.Empty)
                        MessageBox.Show("Error printing " + lbPCBAPrinterFile.Text + " on printer " + Properties.Settings.Default.PcbaPrinterName + "\n" + errorMsg);
                }
                if (Properties.Settings.Default.PFSEnabled)
                {
                    PFS.PSFSendResults(true, pcbaSn, out errorOccured, out errorCode, out errorMsg);
                    if (errorOccured)
                    {
                        MessageBox.Show("There was a error sending the PFS result.");
                        return;
                    }
                }

            }
        }

            void PrintALabel(string labelToPrint, string printerToUse, string pcbaSnToPrint, string hlaSnToPrint, string macToPrint, ref string resultmessage)
        {
            string printedData = string.Empty;
            resultmessage = string.Empty;

            try
            {
                var btengine = new Engine(true);
                var btformat = btengine.Documents.Open(labelToPrint);
                btformat.PrintSetup.PrinterName = printerToUse;
                btformat.PrintSetup.IdenticalCopiesOfLabel = 1;
                foreach (var item in btformat.SubStrings.ToList())
                {
                    if (item.Name == "MAC")
                    {
                        btformat.SubStrings["MAC"].Value = macToPrint;
                        printedData = macToPrint;
                    }
                    else if (item.Name == "SN")
                    {
                        btformat.SubStrings["SN"].Value = hlaSnToPrint;
                        if (printedData != string.Empty)
                            printedData = printedData + ":";
                        printedData = printedData + hlaSnToPrint;
                    }
                    else if (item.Name == "PCBA_SN")
                    {
                        btformat.SubStrings["PCBA_SN"].Value = pcbaSnToPrint;
                        if (printedData != string.Empty)
                            printedData = printedData + ":";
                        printedData = printedData + pcbaSnToPrint;
                    }
                    else if (item.Name == "Product_SN")
                    {
                        btformat.SubStrings["Product_SN"].Value = hlaSnToPrint;
                        if (printedData != string.Empty)
                            printedData = printedData + ":";
                        printedData = printedData + hlaSnToPrint;
                    }
                    else
                    {
                        resultmessage = "System Error: There is a invalid field(" + item.Name + ") in the barcode file.";
                        return;
                    }

                }
                btformat.Save();
                Result result = btformat.Print();
                btformat.Close(SaveOptions.DoNotSaveChanges);
                btengine.Dispose();
            }
            catch (Exception ex)
            {
                resultmessage = "System Error: " + ex.Message;
            }

        }

        private void serversToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ServerSetup iform = new ServerSetup();
            iform.ShowDialog();
            Properties.Settings.Default.ENLServer = iform.Server;
            Properties.Settings.Default.ENLSerialNumberDBName = iform.SNDatabaseName;
            Properties.Settings.Default.ENLProductmapDBName = iform.ProductDatabaseName;
            Properties.Settings.Default.ENLServerUser = iform.User;
            Properties.Settings.Default.ENLServerPassword = iform.Password;
            Properties.Settings.Default.Save();
            iform.Dispose();
        }

        private void printersToolStripMenuItem_Click(object sender, EventArgs e)
        {
            PrinterSetup iform = new PrinterSetup(Properties.Settings.Default.PcbaPrinterName, Properties.Settings.Default.HlaPrinterName);
            iform.ShowDialog();
            Properties.Settings.Default.HlaPrinterName = iform.HlaPrinter;
            Properties.Settings.Default.PcbaPrinterName = iform.PcbaPrinter;
            Properties.Settings.Default.Save();
            iform.Dispose();
        }

        private void pFSToolStripMenuItem_Click(object sender, EventArgs e)
        {
            EditPFSConfig config = new EditPFSConfig(Properties.Settings.Default.PFSServer, Properties.Settings.Default.PFSDatabase, Properties.Settings.Default.PFSWorkCenter, Properties.Settings.Default.PFSEnabled);
            config.ShowDialog();
            Properties.Settings.Default.PFSServer = config.Server;
            Properties.Settings.Default.PFSDatabase = config.DatabaseName;
            Properties.Settings.Default.PFSWorkCenter = config.WorkCenter;
            Properties.Settings.Default.PFSEnabled = config.PFSEnabled;
            Properties.Settings.Default.Save();
            config.Dispose();
        }

        private void stationToolStripMenuItem_Click(object sender, EventArgs e)
        {
            string oldStationName = Properties.Settings.Default.StationName;
            string errorMsg = string.Empty;
            StationSetup stationSetup = new StationSetup();
            stationSetup.ShowDialog();
            Properties.Settings.Default.SensorCOMPort = stationSetup.SensorComPort;
            Properties.Settings.Default.StationName = stationSetup.StationName;
            Properties.Settings.Default.Save();
            stationSetup.Dispose();

            // if station name changed, reload the product combo box
            if (Properties.Settings.Default.StationName != oldStationName)
            {
                ProductDatabase prodDB = new ProductDatabase();
                if (!prodDB.Open(Properties.Settings.Default.ENLServer, Properties.Settings.Default.ENLProductmapDBName, Properties.Settings.Default.ENLServerUser, Properties.Settings.Default.ENLServerPassword))
                {
                    MessageBox.Show("Error opening Productmap database(" + Properties.Settings.Default.ENLProductmapDBName + "). " + prodDB.LastErrorMessage);
                    return;
                }
                if (!prodDB.GetStationProductData(Properties.Settings.Default.StationName, true, out productData, out errorMsg))
                {
                    MessageBox.Show("Error getting products for station " + Properties.Settings.Default.StationName + ". " + errorMsg);
                    return;
                }
                tscbProducts.Items.Clear();
                foreach (var item in productData)
                {
                    tscbProducts.Items.Add(item.product_code);
                }
            }


        }

        private void productSetupToolStripMenuItem_Click(object sender, EventArgs e)
        {
            string errorMsg = string.Empty;

            //ProductDatabase prodDB = new ProductDatabase();
            //if (!prodDB.Open(Properties.Settings.Default.ENLServer, Properties.Settings.Default.ENLProductmapDBName, Properties.Settings.Default.ENLServerUser, Properties.Settings.Default.ENLServerPassword))
            //{
            //    MessageBox.Show("Error opening Productmap database(" + Properties.Settings.Default.ENLProductmapDBName + "). " + prodDB.LastErrorMessage);
            //    return;
            //}
            //if (!prodDB.GetStationProductData(Properties.Settings.Default.StationName, true, out productData, out errorMsg))
            //{
            //    MessageBox.Show("Error getting products for station " + Properties.Settings.Default.StationName + ". " + errorMsg);
            //    return;
            //}
            //tscbProducts.Items.Clear();
            //foreach (var item in productData)
            //{
            //    tscbProducts.Items.Add(item.product_code);
            //}
        }

        private void tscbProducts_SelectedIndexChanged(object sender, EventArgs e)
        {
            lbHLAPrinterLabelFile.Text = "None selected";
            lbPCBAPrinterFile.Text = "None selected";
            lbProduct.Text = "None selected";
            lbPFSAssembly.Text = "None selected";
            PfsAssemblyNumber = string.Empty;

            string modAppVersion = Application.ProductVersion.Substring(0, (Application.ProductVersion.LastIndexOf('.'))); // get rid of build number

            // need to do the find because the combox is sorted, so the index might not match the productData order
            selectedProducData = productData.Find(x => x.pfs_assembly_number == tscbProducts.SelectedItem.ToString().Substring(0, tscbProducts.SelectedItem.ToString().IndexOf(':') - 1));
            if (selectedProducData.limit_version.Substring(0,1) == "[")
            {
                Regex checkver = new Regex(selectedProducData.limit_version.Substring(1, selectedProducData.limit_version.Length - 2));
                if (!checkver.IsMatch(modAppVersion))
                {
                    MessageBox.Show("This is not an allowed version of the program. Program version:" + modAppVersion + "  Allowed version:" + selectedProducData.limit_version);
                    selectedProducData = null;      // clear out selection so it can not be run
                    return;
                }
            }
            else
            {
                if (selectedProducData.limit_version != modAppVersion)
                {
                    MessageBox.Show("This is not an allowed version of the program. Program version:" + modAppVersion + "  Allowed version:" + selectedProducData.limit_version);
                    selectedProducData = null;      // clear out selection so it can not be run
                    return;
                }
            }

            lbHLAPrinterLabelFile.Text = selectedProducData.firmware_id;
            lbPCBAPrinterFile.Text = selectedProducData.firmware_version;
            lbProduct.Text = selectedProducData.product_desc;
            lbPFSAssembly.Text = Properties.Settings.Default.PFSLastOperation + " : " + selectedProducData.pfs_assembly_number;
            PfsAssemblyNumber = selectedProducData.pfs_assembly_number;
            if (selectedProducData.hw_code == (int)GetDataType.Scan)
            { 
                button1.Text = "Scan PCBA or HLA";
                lbChangeoverOption.ForeColor = Color.Red;
                lbChangeoverOption.Text = "Will not changeover at end";
            }
            else
            {
                button1.Text = "Read data from sensor and changeover";
                lbChangeoverOption.ForeColor = Color.Green;
                lbChangeoverOption.Text = "Will changeover at end";
            }
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            string errorMsg = string.Empty;
            bool errorOccured = false;
            int errorCode = 0;
            if (Properties.Settings.Default.PFSEnabled)
            {
                PFS = new PFSInterface(Properties.Settings.Default.PFSServer, Properties.Settings.Default.PFSDatabase, Properties.Settings.Default.PFSWorkCenter);
                if (!PFS.IsPFSInitialized)
                {
                    EditPFSConfig config = new EditPFSConfig(Properties.Settings.Default.PFSServer, Properties.Settings.Default.PFSDatabase, Properties.Settings.Default.PFSWorkCenter, Properties.Settings.Default.PFSEnabled);
                    config.ShowDialog();
                    Properties.Settings.Default.PFSServer = config.Server;
                    Properties.Settings.Default.PFSDatabase = config.DatabaseName;
                    Properties.Settings.Default.PFSWorkCenter = config.WorkCenter;
                    Properties.Settings.Default.PFSEnabled = config.PFSEnabled;
                    Properties.Settings.Default.Save();
                    config.Dispose();
                }
                GetSigninInfo info = new GetSigninInfo(Properties.Settings.Default.PFSLastUser, "", Properties.Settings.Default.PFSLastOperation);
                info.ShowDialog();
                Properties.Settings.Default.PFSLastOperation = info.OperationCode;
                Properties.Settings.Default.PFSLastUser = info.User;
                Properties.Settings.Default.Save();
                PFS.User = info.User;
                PFS.Password = info.Password;
                PFS.OperationCode = info.OperationCode;
               info.Dispose();
                
                
                PFS.PSFLogin(out errorOccured, out errorCode, out errorMsg);
                if (errorOccured)
                {
                    MessageBox.Show("Error loging in. " + errorMsg);
                    lbPFSStatus.ForeColor = Color.Red;
                    lbPFSStatus.Text = "PFS Status - Error logging in.";
                }
                else
                {
                    lbPFSStatus.ForeColor = Color.Green;
                    lbPFSStatus.Text = "PFS Status - Logged in";
                }
            }
            else
            {
                lbPFSStatus.ForeColor = Color.Red;
                lbPFSStatus.Text = "PFS Status - Not enabled";
            }

            // load products for this station
            if (Properties.Settings.Default.StationName != string.Empty)
            {
                ProductDatabase prodDB = new ProductDatabase();
                if (!prodDB.Open(Properties.Settings.Default.ENLServer, Properties.Settings.Default.ENLProductmapDBName, Properties.Settings.Default.ENLServerUser, Properties.Settings.Default.ENLServerPassword))
                {
                    MessageBox.Show("Error opening Productmap database(" + Properties.Settings.Default.ENLProductmapDBName + "). " + prodDB.LastErrorMessage);
                    return;
                }
                if (!prodDB.GetStationProductData(Properties.Settings.Default.StationName, true, out productData, out errorMsg))
                {
                    MessageBox.Show("Error getting products for station " + Properties.Settings.Default.StationName + ". " + errorMsg);
                    return;
                }
                tscbProducts.Items.Clear();
                foreach (var item in productData)
                {
                    tscbProducts.Items.Add(item.pfs_assembly_number + " : " + item.product_desc);
                }
            }
        }

        private void aboutToolStripMenuItem_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Version: " + Application.ProductVersion);
        }

        private void bReprint_Click(object sender, EventArgs e)
        {
            string outmsg = string.Empty;
            string fullLabelName = string.Empty;

            if ((lbHLAPrinterLabelFile.Text == string.Empty) & (lbPCBAPrinterFile.Text == string.Empty))
            {
                MessageBox.Show("No labels have been defined.");
                return;
            }
            if (lbPCBAPrinterFile.Text != string.Empty)
            {
                fullLabelName = Properties.Settings.Default.LabelsDirectory + "\\" + lbPCBAPrinterFile.Text;
                rtbStatus.AppendText("PCBA reprint started....");
                PrintALabel(fullLabelName, Properties.Settings.Default.PcbaPrinterName, lbPCBASN.Text, lbHLASN.Text, lbMac.Text, ref outmsg);
                if (outmsg != string.Empty)
                    MessageBox.Show("Error in printing.\n" + outmsg);
                else
                    rtbStatus.AppendText("Finished\n");
            }
            if (lbHLAPrinterLabelFile.Text != string.Empty)
            {
                outmsg = string.Empty;
                fullLabelName = Properties.Settings.Default.LabelsDirectory + "\\" + lbHLAPrinterLabelFile.Text;
                rtbStatus.AppendText("HLA reprint started....");
                PrintALabel(fullLabelName, Properties.Settings.Default.HlaPrinterName, lbPCBASN.Text, lbHLASN.Text, lbMac.Text, ref outmsg);
                if (outmsg != string.Empty)
                    MessageBox.Show("Error in printing.\n" + outmsg);
                else
                    rtbStatus.AppendText("Finished\n");
            }
        }
    }
}
